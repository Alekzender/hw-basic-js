const arr = [98, 'Have', 76, 'a', 54, 'nice', 32, 'day', 10];
function filterBy(arr, type) {
    let newArr = arr.filter(el => typeof el !== type)
    return newArr
}
console.log(filterBy(arr, 'number'));
const allTypes = ['string', 'number',];
allTypes.forEach(type => console.log(filterBy(arr, type)));