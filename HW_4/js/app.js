let A = prompt('Please, enter first number:');
let B = prompt('Please, enter second number:');
let oper = prompt('Please enter what mathematical operation you want to perform with numbers ' +
    '(you can enter "+" ; "-" ; "*" ; "/") :');

while (!(+A) || !(+B) || oper !== '+' && oper !== '-' && oper !== '*' && oper !== '/') {
    alert('Sorry, but you need to enter a numbers, as well as "+" ; "-" ; "*"; "/" in the corresponding input field. ' +
        'Please try again');
    A = prompt('Please enter the first number again::', A);
    B = prompt('Please enter the second number again:', B);
    oper = prompt('Please enter what mathematical operation you want to perform with numbers ' +
        '(you can enter "+" ; "-" ; "*" ; "/") :', oper);
}

function calculate(numbA, numbB, operation) {
    switch (operation) {
        case '+':
            return ((+numbA) + (+numbB));
        case '-':
            return (numbA - numbB);
        case '*':
            return (numbA * numbB);
        case '/':
            return (numbA / numbB);
    }
}

alert('Check the result in the console' + ': ' + calculate(A, B, oper));

console.log(calculate(A, B, oper));