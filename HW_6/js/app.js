function createNewUser() {
    const newUser = {};
    Object.defineProperties(newUser, {
        firstName: {
            value: prompt('Enter first name:'),
            writable: false,
            configurable: true,
        },
        lastName: {
            value: prompt('Enter last name:'),
            writable: false,
            configurable: true,
        },
        birthday: {
            value: new Date((prompt('Enter your birthday in the format: dd.mm.yyyy').split('.').reverse())),
            writable: false,
            configurable: true,
        },
        getLogin: {
            get: function () {
                return (newUser.firstName[0] + newUser.lastName).toLowerCase();
            },
        },
        getAge: {
            get: function () {
                let now = new Date();
                let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
                let bdCurrentYear = new Date(today.getFullYear(), newUser.birthday.getMonth(), newUser.birthday.getDate());
                let age = today.getFullYear() - newUser.birthday.getFullYear();
                if (today < bdCurrentYear) {
                    age = age - 1;
                }
                return age
            },
        },
        getPassword: {
            get: function () {
                return (newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthday.getFullYear());
            },
        },
    });
    return newUser;
}

let user = createNewUser();
console.log(user.firstName);
console.log(user.lastName);
console.log(user.birthday);
console.log(user);
console.log(user.getLogin);
console.log(user.getAge);
console.log(user.getPassword);

