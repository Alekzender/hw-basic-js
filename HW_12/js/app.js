const btns = document.querySelectorAll('.btn');

document.addEventListener('keyup', changeColor);

function changeColor(event) {
    btns.forEach(element => {
        if (event.code.toLowerCase() === `key${element.dataset.letter.toLowerCase()}` || event.key.toLowerCase() === `${element.dataset.letter.toLowerCase()}`)
        {
            element.style.backgroundColor = '#0000ff';
        } else {
            element.style.backgroundColor = '#000000';
        }
    })
}