function arrayCreate(array, parent = document.body) {
    const ul = document.createElement('ul');
    array.forEach(el => {
        const li = document.createElement('li');
        if (!Array.isArray(el)) {
            li.innerText = el;
        } else {
            arrayCreate(el, li);
        }
        ul.append(li);
    });
    parent.append(ul);
}

const array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const div = document.createElement('div');
document.body.append(div);
arrayCreate(array, div);