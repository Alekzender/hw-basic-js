const tabs = document.querySelector('.tabs');

tabs.addEventListener('click', (event) => {
    const tabsTitle = document.querySelector('.tabs-title.active');
    tabsTitle.classList.remove('active');
    event.target.classList.add('active');
    const tabContent = document.querySelector('.tab-content-text.active');
    tabContent.classList.remove('active');
    const attribute = event.target.getAttribute('data-content');
    const activeTabContent = document.querySelector(`.tab-content-text[data-content=${attribute}]`);
    activeTabContent.classList.add('active');
});