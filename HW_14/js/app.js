const themeBtn = document.getElementById('themeBtn');
const theme = document.getElementById('theme');

function defaultTheme() {
    const previousTheme = localStorage.getItem('theme');
    if (previousTheme === null) {
        applyTheme('style');
    } else {
        applyTheme(previousTheme);
    }
}

function changeTheme() {
    if (theme.getAttribute('href') === './css/green.css') {
        applyTheme('style');
    } else {
        applyTheme('green');
    }
}

function applyTheme(themeName) {
    theme.setAttribute('href', `./css/${themeName}.css`);
    localStorage.setItem('theme', themeName);
}

defaultTheme();
themeBtn.addEventListener('click', changeTheme);