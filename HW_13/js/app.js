const images = document.querySelectorAll('.image-to-show');
const btn = document.querySelector('.btn-wrapper');
const stopBtn = document.querySelector('.stopBtn');
const startBtn = document.querySelector('.startBtn');
let i = 0;

function setDefaultImage() {
    startBtn.disabled = true;
    images[i].style.display = 'block';
    i++;
}

function changeImage() {
    if (i > 3) {i = 0;}
    let activeImage = images[i];
    images.forEach((img) => {
        img.style.display = 'none';
    });
    activeImage.style.display = 'block';
    i++;
}

setDefaultImage();
let timer = setInterval(changeImage, 3000);

function checkEvent(ev) {
    ev.preventDefault();
    ev.target.disabled = true;
    if (ev.target === stopBtn) {
        startBtn.disabled = false;
        clearInterval(timer);
    } else if (ev.target === startBtn) {
        stopBtn.disabled = false;
        timer = setInterval(changeImage, 3000);
    }
}

btn.addEventListener('click', checkEvent);