console.log('Exercise № 1. Numbers that are multiples of 5.');

let number = +prompt('Please, enter a number from 1 to 100:');

while (!Number.isInteger(number) || number > 100 || !number) {
    number = +prompt('Sorry, but you need to pass an integer between 1 and 100:', '100');
}

if (number !== 0 || number >= 5) {
    for (let i = 0; i <= number; i++) {
        if (!(i % 5)) {
            console.log(i);
        }
    }
} else {
    alert('Sorry, no numbers');
}

console.log('Exercise № 2. Output all prime numbers to the console.');

let m = +prompt('Please, enter first number:');
let n = +prompt('Please, enter second number ' +
    '(the second number must be greater than the first "second > first"):');

while (!Number.isInteger(m) || !m || !Number.isInteger(n) || !n || m > n || m === n) {
    alert('Sorry, but you need to pass an integer. ' +
        'The first number must be less than the second number "first < second". Please try again');
    m = +prompt('Please, enter first number:', m);
    n = +prompt('Please, enter second number ' +
        '(the second number must be greater than the first "second > first"):', n);
}

if (m < n && m > 0 && n > 0) {
    for (let i = m; i <= n; i++) {
        let checkPrimeNumber = true;
        for (let j = 2; j < i; j++) {
            if (!(i % j)) {
                checkPrimeNumber = false;
                break;
            }
        }
        if (checkPrimeNumber) {
            console.log(i);
        }
    }
} else {
    alert('Sorry, no matching prime numbers');
}



/*let m = +prompt('Please, enter first number:');
let n = +prompt('Please, enter second number ' +
    '(the second number must be greater than the first "second > first"):');

while (!Number.isInteger(m) || !m || !Number.isInteger(n) || !n || m > n || m === n) {
    alert('Sorry, but you need to pass an integer. ' +
        'The first number must be less than the second number "first < second". Please try again');
    m = +prompt('Please, enter first number:', m);
    n = +prompt('Please, enter second number ' +
        '(the second number must be greater than the first "second > first"):', n);
}

nextPrime:
    for (let i = m; i <= n; i++) {
        for (let j = 2; j < i; j++) {
            if (i % j === 0) continue nextPrime;
        } console.log(i);
    }*/

