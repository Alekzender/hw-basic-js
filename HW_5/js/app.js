function createNewUser() {
    const newUser = {
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        set setFirstName(First_Name) {
            this.firstName = First_Name;
        },
        set setLastName(Last_Name) {
            this.lastName = Last_Name;
        },
    };
    Object.defineProperties(newUser, {
        firstName: {
            value: prompt('Enter first name:'),
            writable: false,
            configurable: true,
        },
        lastName: {
            value: prompt('Enter last name:'),
            writable: false,
            configurable: true,
        },
    });
    return newUser;
}

let user = createNewUser();
console.log(user);
console.log(user.getLogin());

/*
user.firstName = prompt('Enter first name:test-1');//тест перезаписи
user.lastName = prompt('Enter last name:test-1');//тест перезаписи
console.log(user);

user.setFirstName = prompt('Enter first name:test-2');//тест перезаписи
user.setLastName = prompt('Enter last name:test-2');//тест перезаписи
console.log(user);
*/
