const form = document.querySelector('form');
form.addEventListener('click', passwordIsVisible);

function passwordIsVisible (event) {
    if (event.target.tagName.toLowerCase() === 'i') {
        const input = event.target.parentElement.children[0];
        const iconShowPassword = event.target.parentElement.children[1];
        const iconHidePassword = event.target.parentElement.children[2];
        if (input.type === 'password') {
            input.type = 'text';
            iconShowPassword.classList.toggle('active');
            iconHidePassword.classList.toggle('active');
        } else {
            input.type = 'password';
            iconShowPassword.classList.toggle('active');
            iconHidePassword.classList.toggle('active');
        }
    }
}

const submitBtn = document.querySelector('button');
submitBtn.addEventListener('click', checkPassword);

function checkPassword(event) {
    event.preventDefault();
    const inputEnter = document.querySelector('.enter-password');
    const inputConfirm = document.querySelector('.confirm-password');
    const notChecked = document.querySelector('.not-checked');
    if (inputEnter.value === '' && inputConfirm.value === '') {
        notChecked.innerText = 'Потрібно ввести значення!';
    } else {
        if (inputEnter.value !== inputConfirm.value) {
            notChecked.innerText = 'Потрібно ввести однакові значення!';
        } else {
            notChecked.innerText = '';
            setTimeout(() => alert('You are welcome!'), 1);
        }
    }
}
