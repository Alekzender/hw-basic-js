const paragraph = document.querySelectorAll('p');
paragraph.forEach((paragraph) => {
    paragraph.style.backgroundColor = "#ff0000";
});
const idOptList = document.getElementById('optionsList');
console.log(idOptList);
const idOptListParent = idOptList.parentElement;
console.log(idOptListParent);
const idOptListChildrenNodes = idOptList.childNodes;
idOptListChildrenNodes.forEach((childNode) => {
    console.log(`name = ${childNode.nodeName}, type ${childNode.nodeType}.`);
});
const newParagraph = document.getElementById('testParagraph');
newParagraph.innerText = 'This is a paragraph';
const header = document.querySelector('.main-header');
const headerChildren = Array.from(header.children);
headerChildren.forEach((child) => {
    child.classList.add('nav-item');
    console.log(child);
});
const element = document.querySelectorAll('.section-title');
element.forEach((title) => {
    title.classList.remove('section-title');
    console.log(title);
});